import demo.EmployeeRequest;
import demo.EmployeeResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class Employee{

    @Test
    public void getEmployee(){
        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-type", "application/json")
                .header("Accept", "*/*")
                .get("v1/employees");

        response.getBody().prettyPrint();
        System.out.println(response.getStatusCode());
        Assert.assertEquals(200, response.getStatusCode());
        Assert.assertThat("kelamaan ah", response.getTime(), Matchers.lessThan(4000L));
        System.out.println("sistem udah cepet nih");
        Assert.assertEquals("success", response.path("status"));

        Assert.assertEquals("Tiger Nixon", response.path("data[0].employee_name"));

        EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);
        //serializer = memasukkan body response ke EmployeeResponse
        System.out.println(employeeResponse.getData().get(0).getEmployeeName());
    }

    @Test
    public void createEmployee(){
        /*String requestBody = "{\n" +
                "  \"name\": \"Dana\",\n" +
                "  \"salary\": \"123\",\n" +
                "  \"age\": \"23\"\n" +
                "}";       cara pertama*/
        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setName("Danan");
        employeeRequest.setAge("22");
        employeeRequest.setSalary("7500");

        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-type", "application/json")
                .header("Accept", "*/*")
                /*.body(requestBody)    cara pertama*/
                .body(employeeRequest)
                .post("v1/create");
        System.out.println("hasil dari .log dan .all");
        response.getBody().prettyPrint();
        System.out.println("hasil dari getbody dan prettyprint");
    }
}